<?php

if (isset($_POST['filePath']) && isset($_POST['format']) && isset($_POST['resolution'])) {
    $videoPath = $_POST['filePath'];
    $videoFormat = $_POST['format'];
    $videoResolution = $_POST['resolution'];
    $videoWidthHeight = explode('x', $videoResolution);
    $videoWidth = $videoWidthHeight[0];
    $videoHeight = $videoWidthHeight[1];
    $bitrate = $videoWidth * $videoHeight * 30;

    try {

        if (file_exists("resultVideos/resultVideo." . $videoFormat)) {
            unlink("resultVideos/resultVideo." . $videoFormat);
        }

        exec("ffmpeg -i " . $videoPath . " -f " . $videoFormat . " -ac 2 -ab 96k -ar 44100 -b " . $bitrate . " -s " . $videoResolution . " resultVideos/resultVideo." . $videoFormat, $out, $res);

        if ($res === 0) {
            echo '
            <video controls="controls" src="resultVideos/resultVideo.' . $videoFormat . '" autoplay>
                Тег video не поддерживается вашим браузером.
            </video>';
        } else {
            throw new Exception('Video Encoding Error');
        }

    } catch (Exception $e) {
        echo $e->getMessage(), "\n";;
    }

}